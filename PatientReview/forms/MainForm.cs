﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientReview.forms;

namespace PatientReview
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void addPatientButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddPatient nextView = new AddPatient(null);
            nextView.ShowDialog();
            this.Close();
        }

        private void findPatientButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            FindBox nextView = new FindBox();
            nextView.ShowDialog();
            this.Close();
        }

        private void editDoctors_Click(object sender, EventArgs e)
        {
            DoctorEditor nextView = new DoctorEditor();
            nextView.ShowDialog();
            this.Close();
        }

        private void generateRaport_Click(object sender, EventArgs e)
        {
            ReportGenerator.generate(this.przychodnia1);
        }
    }
}
