﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using PatientReview.Datasets;
using PatientReview.Datasets.PrzychodniaTableAdapters;

namespace PatientReview.forms
{
    public partial class AddPatient : Form {


        public AddPatient(string patientId)
        {
            InitializeComponent();
            savePanel.Show();
        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            int result = this.patientTableAdapter.Update(this.PrzychodniaDataSet.Patient);
            result += this.deklaracjeTableAdapter1.Update(this.PrzychodniaDataSet.Deklaracje);
            MessageBox.Show("Saved " + result + "row.");
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            switchToMainView();
        }

        private void switchToMainView()
        {
            this.Hide();
            MainForm nextForm = new MainForm();
            nextForm.ShowDialog();
            this.Close();
        }

        private void AddPatient_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'PrzychodniaDataSet.Visits' table. You can move, or remove it, as needed.
            this.visitsTableAdapter.Fill(this.PrzychodniaDataSet.Visits);
            // TODO: This line of code loads data into the 'PrzychodniaDataSet.Patient' table. You can move, or remove it, as needed.
            this.patientTableAdapter.Fill(this.PrzychodniaDataSet.Patient);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddVisit nextView = new AddVisit();
            nextView.ShowDialog();
            this.Close();
        }
    }
}
