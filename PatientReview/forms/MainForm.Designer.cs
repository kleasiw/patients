﻿namespace PatientReview
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addPatientButton = new System.Windows.Forms.Button();
            this.findPatientButton = new System.Windows.Forms.Button();
            this.generateRaport = new System.Windows.Forms.Button();
            this.editDoctors = new System.Windows.Forms.Button();
            this.przychodnia1 = new PatientReview.Datasets.Przychodnia();
            ((System.ComponentModel.ISupportInitialize)(this.przychodnia1)).BeginInit();
            this.SuspendLayout();
            // 
            // addPatientButton
            // 
            this.addPatientButton.Location = new System.Drawing.Point(29, 12);
            this.addPatientButton.Name = "addPatientButton";
            this.addPatientButton.Size = new System.Drawing.Size(430, 28);
            this.addPatientButton.TabIndex = 0;
            this.addPatientButton.Text = "Dodaj/Edytuj pacjentów";
            this.addPatientButton.UseVisualStyleBackColor = true;
            this.addPatientButton.Click += new System.EventHandler(this.addPatientButton_Click);
            // 
            // findPatientButton
            // 
            this.findPatientButton.Location = new System.Drawing.Point(29, 46);
            this.findPatientButton.Name = "findPatientButton";
            this.findPatientButton.Size = new System.Drawing.Size(430, 29);
            this.findPatientButton.TabIndex = 1;
            this.findPatientButton.Text = "Znajdz pacjenta";
            this.findPatientButton.UseVisualStyleBackColor = true;
            this.findPatientButton.Click += new System.EventHandler(this.findPatientButton_Click);
            // 
            // generateRaport
            // 
            this.generateRaport.Location = new System.Drawing.Point(29, 176);
            this.generateRaport.Name = "generateRaport";
            this.generateRaport.Size = new System.Drawing.Size(430, 28);
            this.generateRaport.TabIndex = 2;
            this.generateRaport.Text = "Generuj raport";
            this.generateRaport.UseVisualStyleBackColor = true;
            this.generateRaport.Click += new System.EventHandler(this.generateRaport_Click);
            // 
            // editDoctors
            // 
            this.editDoctors.Location = new System.Drawing.Point(29, 81);
            this.editDoctors.Name = "editDoctors";
            this.editDoctors.Size = new System.Drawing.Size(430, 29);
            this.editDoctors.TabIndex = 3;
            this.editDoctors.Text = "Edytuj lekarzy";
            this.editDoctors.UseVisualStyleBackColor = true;
            this.editDoctors.Click += new System.EventHandler(this.editDoctors_Click);
            // 
            // przychodnia1
            // 
            this.przychodnia1.DataSetName = "Przychodnia";
            this.przychodnia1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(485, 225);
            this.Controls.Add(this.editDoctors);
            this.Controls.Add(this.generateRaport);
            this.Controls.Add(this.findPatientButton);
            this.Controls.Add(this.addPatientButton);
            this.Name = "MainForm";
            this.Text = "PatientReview";
            ((System.ComponentModel.ISupportInitialize)(this.przychodnia1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addPatientButton;
        private System.Windows.Forms.Button findPatientButton;
        private System.Windows.Forms.Button generateRaport;
        private System.Windows.Forms.Button editDoctors;
        private Datasets.Przychodnia przychodnia1;
    }
}

