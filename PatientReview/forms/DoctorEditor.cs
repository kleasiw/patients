﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientReview.forms
{
    public partial class DoctorEditor : Form
    {
        public DoctorEditor()
        {
            InitializeComponent();
        }

        private void DoctorEditor_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'przychodnia.Doctors' table. You can move, or remove it, as needed.
            this.doctorsTableAdapter.Fill(this.przychodnia.Doctors);
        }

        private void saveChanges_Click(object sender, EventArgs e)
        {
            int result = this.doctorsTableAdapter.Update(this.przychodnia.Doctors);
            MessageBox.Show("Saved " + result + "row.");
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            switchToMainView();
        }

        private void switchToMainView()
        {
            this.Hide();
            MainForm nextForm = new MainForm();
            nextForm.ShowDialog();
            this.Close();
        }
    }
}
