﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientReview.Datasets;
using System.Data.OleDb;

namespace PatientReview.forms
{
    public partial class FindBox : Form
    {
        // TODO: sometimes cell not clicked
        public FindBox()
        {
            InitializeComponent();
        }

        private void FindBox_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'przychodnia.Deklaracje' table. You can move, or remove it, as needed.
            this.deklaracjeTableAdapter.Fill(this.przychodnia.Deklaracje);
           // this.dataGridView2.ClearSelection();
            // TODO: This line of code loads data into the 'przychodnia.Patient' table. You can move, or remove it, as needed.
            this.patientTableAdapter.Fill(this.przychodnia.Patient);

        }

        private void findButton_Click(object sender, EventArgs e)
        {
            this.przychodnia.Deklaracje.RejectChanges();
            this.dataGridView1.ClearSelection();
            this.dataGridView2.ClearSelection();


            DataView dv = new DataView(this.przychodnia.Patient);
            DataView viewDeklaracje = new DataView(this.przychodnia.Deklaracje);
            setFilter(dv);
            if (dv.Count != 0)
            {
                //viewDeklaracje.RowFilter = this.przychodnia.Deklaracje.IdPacjentaColumn.ColumnName + " = '" + dv[0][0] + "'";
                viewDeklaracje.Sort = this.przychodnia.Deklaracje.DataColumn.ColumnName + " DESC";

                while (viewDeklaracje.Count > 1)
                    viewDeklaracje.Delete(1);
                dataGridView2.DataSource = viewDeklaracje;
            }
            else {
                this.dataGridView2.DataSource = null;
            }

            dataGridView1.DataSource = dv;
            this.dataGridView1.Show();
            this.dataGridView2.Show();
        }

        private void setFilter(DataView dv)
        {
            if (isEmptyField(lastNameBox))
            {
                if (isEmptyField(peselBox))
                {
                    dv.RowFilter = null;//do nothing or show everything
                    //dv.RowFilter = this.przychodnia.Patient.PESELColumn.ColumnName + " = '" + peselBox.Text + "'";
                }
                else
                {
                    dv.RowFilter = this.przychodnia.Patient.PESELColumn.ColumnName + " = '" + peselBox.Text + "'";
                }
            }
            else 
            {
                if (isEmptyField(peselBox))
                {
                    dv.RowFilter = this.przychodnia.Patient.NazwiskoColumn.ColumnName + " = '" + lastNameBox.Text + "'";

                }
                else
                { 
                    dv.RowFilter = this.przychodnia.Patient.NazwiskoColumn.ColumnName + " = '" + lastNameBox.Text + "' AND "
                           + this.przychodnia.Patient.PESELColumn.ColumnName + " = '" + peselBox.Text + "'";
                }
            }
        }

        private bool isEmptyField(TextBox box)
        {
            return box.Text == null || box.Text == "";
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
          
            int index = e.RowIndex;
            if (index >= 0)
            {
                int pacientId = (int)dataGridView1.Rows[index].Cells[0].Value;

                StringBuilder sb = new StringBuilder();
                sb.Append(this.przychodnia.Deklaracje.IdPacjentaColumn);
                sb.Append(" = '");
                sb.Append(pacientId);
                sb.Append("'");
                DataView patients = new DataView(this.przychodnia.Patient);
                DataView declaration = new DataView(this.przychodnia.Deklaracje);
                declaration.RowFilter = sb.ToString();
                this.dataGridView2.DataSource = declaration;
                this.dataGridView2.Show();
            }
            
        }

        private void toMainViewButton_Click(object sender, EventArgs e)
        {
            switchToMainView();
        }


        private void switchToMainView()
        {
            this.Hide();
            MainForm nextForm = new MainForm();
            nextForm.ShowDialog();
            this.Close();
        }
    }
}
