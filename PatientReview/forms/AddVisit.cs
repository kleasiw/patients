﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientReview.forms
{
    public partial class AddVisit : Form
    {
        public AddVisit()
        {
            InitializeComponent();
        }

        private void AddVisit_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'PrzychodniaDataSet.Patient' table. You can move, or remove it, as needed.
            this.patientTableAdapter.Fill(this.PrzychodniaDataSet.Patient);
            // TODO: This line of code loads data into the 'PrzychodniaDataSet.Doctors' table. You can move, or remove it, as needed.
            this.doctorsTableAdapter.Fill(this.PrzychodniaDataSet.Doctors);
            // TODO: This line of code loads data into the 'PrzychodniaDataSet.Doctors' table. You can move, or remove it, as needed.
            this.doctorsTableAdapter.Fill(this.PrzychodniaDataSet.Doctors);
            // TODO: This line of code loads data into the 'przychodnia.ICD' table. You can move, or remove it, as needed.
            this.iCDTableAdapter.Fill(this.PrzychodniaDataSet.ICD);
            // TODO: This line of code loads data into the 'przychodnia.Visits' table. You can move, or remove it, as needed.
            this.visitsTableAdapter.Fill(this.PrzychodniaDataSet.Visits);

        }

        private void save_Click(object sender, EventArgs e)
        {
            int result = this.visitsTableAdapter.Update(this.PrzychodniaDataSet.Visits);
            MessageBox.Show("Saved " + result + " rows");

        }

        private void cancel_Click(object sender, EventArgs e)
        {
            switchToMainView();
        }

        private void switchToMainView()
        {
            this.Hide();
            MainForm nextForm = new MainForm();
            nextForm.ShowDialog();
            this.Close();
        }
    }
}
