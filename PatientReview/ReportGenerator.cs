﻿using ICSharpCode.SharpZipLib.Zip;
using PatientReview.Datasets;
using PatientReview.Datasets.PrzychodniaTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PatientReview.Datasets.Przychodnia;

namespace PatientReview
{
    class ReportGenerator
    {
        private static Przychodnia przychodniaDataSet;

        public static void generate(Przychodnia przychodnia) {
            przychodniaDataSet = przychodnia;
            PatientTableAdapter pacjenciTableAdapter = new PatientTableAdapter();
            DeklaracjeTableAdapter deklaracjeTableAdapter = new DeklaracjeTableAdapter();
            DoctorsTableAdapter lekarzeTableAdapter = new DoctorsTableAdapter();


            StringBuilder builder = new StringBuilder();
            builder.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
            builder.Append("<Deklaracje>\n");
            pacjenciTableAdapter.Fill(przychodnia.Patient);
            deklaracjeTableAdapter.Fill(przychodnia.Deklaracje);
            lekarzeTableAdapter.Fill(przychodnia.Doctors);

           
            DataView declarations = new DataView(przychodniaDataSet.Deklaracje);
            declarations.RowFilter = przychodniaDataSet.Deklaracje.PozytywnaColumn.ColumnName + "=" + "true";
            foreach (DataRow row in declarations.ToTable().Rows)
            {
                DataView patients = insertPatients(builder, row);
                insertPersonel(builder, declarations, row, patients);
                builder.Append("</Deklaracja>");

            }

            builder.Append("</Deklaracje>");
            zip(builder.ToString());
        }

        private static void insertPersonel(StringBuilder builder, DataView declarations, DataRow declarationRow, DataView patients)
        {
            DataView doctors = new DataView(przychodniaDataSet.Doctors);
            patients.RowFilter = przychodniaDataSet.Doctors.IDColumn.ColumnName + " = " + declarationRow[2].ToString();

            builder.Append("\t\t<NPWZ>");
            builder.Append(doctors[0][przychodniaDataSet.Doctors.NPWZColumn.ColumnName]);
            builder.Append("</NPWZ>\n\t\t<Data>");
            builder.Append(declarationRow[3]);
            builder.Append("</Data>\n");
            builder.Append("\t</Personel>\n");
        }

        private static DataView insertPatients(StringBuilder builder, DataRow declarationRow)
        {
            DataView patients = new DataView(przychodniaDataSet.Patient);
            patients.RowFilter = przychodniaDataSet.Patient.IDColumn.ColumnName + " = " + declarationRow[1].ToString();

            builder.Append("<Deklaracja>\n\t<Pacjent>\n\t\t<Nazwisko>");
            builder.Append(patients[0][przychodniaDataSet.Patient.NazwiskoColumn.ColumnName]);
            builder.Append("</Nazwisko>\n\t\t<Imiona>");
            builder.Append(patients[0][przychodniaDataSet.Patient.ImionaColumn.ColumnName]);
            builder.Append("</Imiona>\n\t\t<PESEL>");
            builder.Append(patients[0][przychodniaDataSet.Patient.PESELColumn.ColumnName]);
            builder.Append("</PESEL>\n\t\t<DataUrodzenia>");
            builder.Append(patients[0][przychodniaDataSet.Patient.DataUrodzeniaColumn.ColumnName]);
            builder.Append("</DataUrodzenia>\n\t\t<Ubezpieczenie>");
            builder.Append(patients[0][przychodniaDataSet.Patient.UbezpieczenieColumn.ColumnName]);
            builder.Append("</Ubezpieczenie>\n\t\t<Adres>\n\t\t<Ulica>");
            builder.Append(patients[0][przychodniaDataSet.Patient.UlicaColumn.ColumnName]);
            builder.Append("</Ulica>\n\t\t<NrDomu>");
            builder.Append(patients[0][przychodniaDataSet.Patient.NrDomuColumn.ColumnName]);
            builder.Append("</NrDomu>\n\t\t<NrMieszkania>");
            builder.Append(patients[0][przychodniaDataSet.Patient.NrMieszkaniaColumn.ColumnName]);
            builder.Append("</NrMieszkania>\n\t\t<Miasto>");
            builder.Append(patients[0][przychodniaDataSet.Patient.MiastoColumn.ColumnName]);
            builder.Append("</Miasto>\n\t\t<Kod>");
            builder.Append(patients[0][przychodniaDataSet.Patient.KodColumn.ColumnName]);
            builder.Append("</Kod>\n\t\t</Adres>\n\t</Pacjent>\n\t<Personel>\n");
            return patients;
        }

        private static void zip(string outputString)
        {
            String date = DateTime.Now.ToString("yyyy_MM_dd_HH_mm");
            using (FileStream fileOut = File.Create("Raport_"+date+".zip"))
            {
                using (ZipOutputStream zipOut = new ZipOutputStream(fileOut))
                {
                    zipOut.SetLevel(3);
                    ZipEntry zipEntry = new ZipEntry("Raport_" +
                        date + ".xml");
                    zipEntry.DateTime = DateTime.UtcNow;
                    zipOut.PutNextEntry(zipEntry);

                    byte[] buffer = Encoding.UTF8.GetBytes(outputString);
                    zipOut.Write(buffer, 0, buffer.Length);

                    zipOut.IsStreamOwner = false;
                    zipOut.Close();
                }
            }
        }
    }
}
